package TradeViewer;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * add comment here...
 */
class ScatterplotV extends JFrame {

    private ListPanel listPanel;
    private ScatterplotPanel scatterplotPanel;
    private SortPanel sortPanel;
    private RecordPanel recordPanel;
    private Model model;
    private TablePanel tablePanel;
    
    public ScatterplotV(String filename) {
        model = new Model(filename);//will create the model

        addWindowListener(new WindowCloser());

        // add the main Swing components
        sortPanel = new SortPanel(model); //This panel was added to implement sorting
        tablePanel = new TablePanel(model);//This is the table that contains all data
        									//and shows selections
        model.addChild(tablePanel);	
        listPanel = new ListPanel(model);
        model.addChild(listPanel);
        recordPanel = new RecordPanel(model);
        model.addChild(recordPanel);
        scatterplotPanel = new ScatterplotPanel(model);
        model.addChild(scatterplotPanel);

        // prep component layout
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add("North", sortPanel);
        contentPane.add("South", tablePanel);
        contentPane.add("Center", scatterplotPanel);
        

        

        final int DEFAULT_FRAME_WIDTH = 550;
        final int DEFAULT_FRAME_HEIGHT = 800;

        setSize(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);
        setTitle("WHO Olympics Data");
        setVisible(true);

    }
    
   

    private class WindowCloser extends WindowAdapter {

        public void windowClosing(WindowEvent event) {
            System.exit(0);
        }
    	}
} /* end TradeViewerFrame */
