package TradeViewer;
/*
SortPanel.java

This is a panel that contains tools for sorting data


* @author Vladislavs Zaharovs (some code was taken from Java tutorials)
*/
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import TradeViewer.ScatterplotPanel.ButtonListener;
import TradeViewer.ScatterplotPanel.SliderListener;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class SortPanel extends JPanel{

    private Model model; 
    private JButton reset, selectAll; // clears model.selectedRows, repaints
    
    
    public SortPanel(Model m) {
        this.model = m;       
        
        // Create reset button and add listeners
        reset = new JButton("Reset"); 
        selectAll = new JButton("Select All");
        reset.addActionListener(new ButtonListener());
        selectAll.addActionListener(new ButtonListener());
        
		 ///rest slider panel
        JPanel sliderPanel = new JPanel();
        sliderPanel.setLayout(new GridLayout(1, 2, 2, 2));
        ///
        ///Button panel
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 2, 2, 2));
        buttonPanel.add(selectAll);
        buttonPanel.add(reset);
        buttonPanel.setPreferredSize(new Dimension(80,40));
        ///
        
        
        /* Set up a layout manager to handle this panel's subcomponents */
        setLayout(new BorderLayout());
        add("South", buttonPanel);
    }
    

    //Actionlistener for the buttons to move the view
    // invokes the Model methods
    public class ButtonListener implements ActionListener{
  	  public void actionPerformed(ActionEvent e){

  		JButton b = (JButton) e.getSource();
  		if (b.equals(reset)) {
  			model.resetAll();
  		}
  		else model.selectAll();
  		model.updateAll();
  		}
  	  }
    
}
