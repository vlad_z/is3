There are 3 example graphs.
To run them you will require JavaFX. Check your Java version, if you
have Java ver1.7 you will have JavaFX. If not, go download Java 1.7!


If you are running a Linux or OSX, run the specific shell script in
the 'unix' directory using the terminal (./runBars.sh). 

If you are running Windows, run the specific batch file in the
'windows' directory.

The java code can be found in src/JavaFX.java
