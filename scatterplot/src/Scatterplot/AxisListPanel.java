package Scatterplot;

/*
  AxisListPanel.java
  
   Has been changed to have JComboBoxes for choosing
   the axis. It is technically an AxisComboBoxPanel
   but I retained the old name 
*/

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

class AxisListPanel extends JPanel {
	 private ScatterplotPanel parent;
	 private JComboBox xBox, yBox; //NEW

	 public AxisListPanel(ScatterplotPanel p, ArrayList v) {
		 
		  parent = p;
		  setLayout(new GridLayout(2,1));
		  
		  xBox = new JComboBox(v.toArray());
		  yBox = new JComboBox(v.toArray());
		  xBox.setPreferredSize(new Dimension(160, 30));
		  yBox.setPreferredSize(new Dimension(160, 30));
		  
	       xBox.setSelectedIndex(0);
	       yBox.setSelectedIndex(1);
	       
	       ComboListener comboListener = new ComboListener();
	       xBox.addActionListener(comboListener);
	       yBox.addActionListener(comboListener);

		  add(xBox);
		  add(yBox);
  }
	 
	 

	 // This is the action listener for the combo boxes
	 //invokes the parent.updateYAxis(String s) or parent.updateXAxis(String s)
	 private class ComboListener implements ActionListener{
	    public void actionPerformed(ActionEvent e) {
	        JComboBox cb = (JComboBox)e.getSource();
	        String label = (String)cb.getSelectedItem();
			 if (label != null) {
				  if (cb == xBox) {
						//System.out.println ("X: " + (String) label);
						parent.updateXAxis ((String) label);
				  }
				  else { // yList
						//System.out.println ("Y: " + (String) label);
						parent.updateYAxis ((String) label);
				  }
			 }

	    }}

}
