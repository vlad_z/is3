package Scatterplot;

// Interface for MVC protocol. Each VC pair should implement this
//
// To support multiple selections the public method void update(int row)
// was replaced with one that has no parameters, so it should update based on
// the models selectedRows ArrayList

public interface ViewController {
  public void update (); //This method will update using the models 
}

