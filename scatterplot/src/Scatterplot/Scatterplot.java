package Scatterplot;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/*
 * add comment here...
 */
class Scatterplot extends JPanel {

    private ListPanel listPanel;
    private ScatterplotPanel scatterplotPanel;
    private SortPanel sortPanel;
    private RecordPanel recordPanel;
    private Model model;
    private TablePanel tablePanel;
    
    public Scatterplot(String filename) {
        model = new Model(filename);//will create the model

        // add the main Swing components
        sortPanel = new SortPanel(model); //This panel was added to implement sorting
        tablePanel = new TablePanel(model);//This is the table that contains all data
        									//and shows selections
        model.addChild(tablePanel);	
        listPanel = new ListPanel(model);
        model.addChild(listPanel);
        recordPanel = new RecordPanel(model);
        model.addChild(recordPanel);
        scatterplotPanel = new ScatterplotPanel(model);
        model.addChild(scatterplotPanel);

        // prep component layout
        JPanel buttons = new JPanel(new BorderLayout(2,1));
        buttons.add("North", sortPanel);
        buttons.add("Center", tablePanel);
        
        //Container contentPane = getContentPane();
        this.setLayout(new BorderLayout());
        this.add("West", buttons);
        this.add("Center", scatterplotPanel);
        

        

        final int DEFAULT_FRAME_WIDTH = 1000;
        final int DEFAULT_FRAME_HEIGHT = 400;

        setSize(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);


    }
    
   


} 
