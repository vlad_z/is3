package Scatterplot;

/*
 ScatterplotView.java

 Scatterplot for displaying financial data
 
 The class is updated to support multiple selection,
 specifically the paint() method
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Enumeration;

class ScatterplotView extends JPanel {

    private Point axisOrigin;
    private int width, height;
    private double xScale, yScale;
    private double cacheX[], cacheY[];
    private int selected;
    private Model model;
    private int xPan = -300, yPan = 110; //values to adjust the focus of the plot
    
    
    //Set up the methods for getting and setting the focus
    public void setXP(int x){xPan = x;}
    public void setYP(int y){yPan = y;}
    public int getXP(){return xPan;}
    public int getYP(){return yPan;}

    public ScatterplotView(Model model, double scale) {
        setSize(new Dimension(600, 600));
        // initialise origin to midpoint of panel
        axisOrigin = new Point();
        // initialise scales
        xScale = scale;
        yScale = scale;
        // initialise data
        this.model = model;
        int dataSize = model.dataSize();
        cacheX = new double[dataSize];
        cacheY = new double[dataSize];
        loadData(3, 4);
        selected = -1;
        
        //works with mouse clicks
        addMouseListener(new MouseButtonHandler());
    }

    class MouseButtonHandler extends MouseAdapter {

        public void mouseReleased(MouseEvent me) {
        	
            int row = dotSelected(new Point(me.getX(), me.getY()));
            // System.out.println("ScatterplotView.MouseButtonHandler: " + row);
            if (row >= 0) {
                model.selected(row);
            }
        }
    }

    int dotSelected(Point p) {
        int found = -1;
        int max = model.dataSize();
        int i = 0;
        while ((i < max) && (found == -1)) {
            if ((Math.abs(p.getX() - (axisOrigin.getX()
                    + (int) (cacheX[i] * xScale))) <= 2)
                    & (Math.abs(p.getY() - (axisOrigin.getY()
                    - (int) (cacheY[i] * yScale))) <= 2)) {
                found = i;//changes if mouse cursos matches one of the points
            } else {
                i++;
            }
        }
        return found;
    } // dotSelected

    public void paint(Graphics g) {
    	model.updateAll();
        double x, y;
        // get panel dimensions
        height = getSize().height;
        width = getSize().width;
        // fix nasty redraw bug
        g.setColor(Color.lightGray);
        g.fillRect(0, 0, width, height);
        // set axis to center
        axisOrigin.setX(width / 2 + xPan); //xPan will move the focus up
        axisOrigin.setY(height / 2 + yPan);//yPan will move the focus left
        // draw the axes
        g.setColor(Color.blue);
        g.drawLine(0, axisOrigin.getY(), width, axisOrigin.getY());
        g.drawLine(axisOrigin.getX(), 0, axisOrigin.getX(), height);
        // draw all the elements except the selected one
        for (int i = 0; i < model.dataSize(); i++) {
        	//paint unselected dots red
            if (!model.isSelected(i)) {
                g.setColor(Color.red);
                g.fillOval(axisOrigin.getX() + (int) (cacheX[i] * xScale) - 2,
                        axisOrigin.getY() - (int) (cacheY[i] * yScale) - 2,
                        4, 4);
            }
            else{//paint selected dots green
                g.setColor(Color.green);
                g.fillOval(axisOrigin.getX() + (int) (cacheX[i] * xScale) - 2,
                        axisOrigin.getY() - (int) (cacheY[i] * yScale) - 2,
                        4, 4);
            }
        }
    }

    void loadData(int xAxisField, int yAxisField) {
        // construct a scatterplot mapping data in field with index xAxisField
        // to the x axis and data in field yAxisField to the y axis
        for (int i = 0; i < model.dataSize(); i++) {
            cacheX[i] = model.fieldAsReal(i, xAxisField);
            cacheY[i] = model.fieldAsReal(i, yAxisField);
        }
        repaint();
    }

    // updating methods
    public void itemSelected(int pos) {
        selected = pos;
        repaint();
    }
    
    //The ViewController method
    public void update() {;
        repaint();
    }

    public void setXScale(double val) {
        xScale = val;
        repaint();
    }

    public void setYScale(double val) {
        yScale = val;
        repaint();
    }
} // end ScatterplotPanel
