package application;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javafx.embed.swing.JFXPanel;
import java.awt.*;
import javafx.application.Platform;
import javafx.scene.*;
import java.net.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import java.util.*;
import Scatterplot.*;

/** @author MichaelRoddy
 *  Loads a java window displaying a D3 graph via a html web page   
 */
public class Viewer implements ActionListener {
	   
		private static Main_Model model;
	    // Used to open the graph
	    private static String openPage = "/src/graphs/line.html";
	    private static String dir = "";
	    
	    // Used for creating correctly sized views
	    private static Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
	    private static int numViews;
	    
	    // Start application
	    public static void main(final String[] args) {

	    	model = new Main_Model();
	    	
	        dir = System.getProperty("user.dir");
	        
	        // Adjust screen size to deal with taskbar
	        screenSize.setSize(screenSize.width, screenSize.height);
	        
	        // Run
	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                initAndShowGUI();
	            }
	        });
	    }
	    
	    // Create a JFrame with a JButton and a JFXPanel containing the WebView.
	    private static void initAndShowGUI(){
	    	
	        final JFXPanel fxPanel = new JFXPanel();
	        final JFXPanel fxPanel2 = new JFXPanel();
	        final JFXPanel fxPanel4 = new JFXPanel();
	        

	    	JMenuBar menuBar;
	    	JMenu fileMenu, menu, lSubmenu, bSubmenu, cSubmenu, rSubmenu;
	    	JMenuItem exitMenuItem;
	    	JMenuItem pMenuItemPop, pMenuItemGdp, pMenuItemVar3, pMenuItemVar4;
	    	JMenuItem cMenuItemPop, cMenuItemGdp, cMenuItemVar3, cMenuItemVar4;
	    	
	    	menuBar = new JMenuBar();
	    	menu = new JMenu("Graphs");
	    	fileMenu = new JMenu("File");
	    	menuBar.add(fileMenu);
	    	menuBar.add(menu);
	    	lSubmenu = new JMenu("Polar Diagram");
	    	bSubmenu = new JMenu("Bubble chart");
	    	cSubmenu = new JMenu("Choropleth");
	    	rSubmenu = new JMenu("Ranking graph");
	    	
	    	exitMenuItem = new JMenuItem("Exit");
	        exitMenuItem.addActionListener(new ActionListener() {
	            @Override public void actionPerformed( ActionEvent aActionEvent ) {
	              System.exit(0);
	            }
	        });
	        fileMenu.add(exitMenuItem);
	    	
	    	pMenuItemPop = new JMenuItem("Variable: population");
	        pMenuItemPop.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("polar:population");
	                ArrayList<String> f = new ArrayList<String>();
	                f.add("population total");
	                f.add("gold");
	                model.export("polar", f);
	                initAndShowGUI();
	            }
	        });
	    	lSubmenu.add(pMenuItemPop);
	    	pMenuItemGdp = new JMenuItem("Variable: gdp");
	        pMenuItemGdp.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("polar:gdp2011");
	                ArrayList<String> f = new ArrayList<String>();
	                f.add("gdp2011");
	                f.add("gold");
	                model.export("polar", f);
	                initAndShowGUI();
	            }
	        });
	    	lSubmenu.add(pMenuItemGdp);
	    	pMenuItemVar3 = new JMenuItem("Variable: adult literacy rate");
	        pMenuItemVar3.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("polar:adult literacy rate");
	                ArrayList<String> f = new ArrayList<String>();
	                f.add("adult literacy rate");
	                f.add("gold");
	                model.export("polar", f);
	                initAndShowGUI();
	            }
	        });
	    	lSubmenu.add(pMenuItemVar3);
	    	pMenuItemVar4 = new JMenuItem("Variable: team size");
	        pMenuItemVar4.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("polar:teamsize");
	                ArrayList<String> f = new ArrayList<String>();
	                f.add("teamsize");
	                f.add("gold");
	                model.export("polar", f);
	                initAndShowGUI();
	            }
	        });
	    	lSubmenu.add(pMenuItemVar4);
	    	
	    	
	    	cMenuItemPop = new JMenuItem("Variable: population");
	        cMenuItemPop.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("choropleth:population");
	            }
	        });
	    	cSubmenu.add(cMenuItemPop);
	    	cMenuItemGdp = new JMenuItem("Variable: gdp");
	        cMenuItemGdp.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("choropleth:gdp");
	            }
	        });
	    	cSubmenu.add(cMenuItemGdp);
	    	cMenuItemVar3 = new JMenuItem("Variable: something else");
	        cMenuItemVar3.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("choropleth:var3");
	            }
	        });
	    	cSubmenu.add(cMenuItemVar3);
	    	cMenuItemVar4 = new JMenuItem("Variable: another thing");
	        cMenuItemVar4.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("choropleth:var4");
	            }
	        });
	    	cSubmenu.add(cMenuItemVar4);

	    	menu.add(lSubmenu);
	    	menu.add(cSubmenu);
	    	
	    	int buffer = 10;
	    	GridLayout layoutManager = new GridLayout(2,1);
	    	
	        // This method is invoked on Swing thread
	        JFrame frame = new JFrame("FX");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        // Do the layout manually
	        frame.getContentPane().setLayout(layoutManager);
	        final JButton jButton = new JButton("Random Buttom");

	        
	    
	        
	        //frame.add(jButton);
	        frame.add(fxPanel);
	        frame.setVisible(true);
	        numViews++;
	       
	        frame.add(fxPanel2);
	        frame.add(new Scatterplot(dir + "/src/Scatterplot/who.csv"));
	        //frame.add(fxPanel4);
	        
	        jButton.setSize(new Dimension(200, 27));
	        
	        // These are just random sizes. If you change these, you have to change
	        // them below too AND in each D3.html
	        fxPanel.setSize(new Dimension((int)screenSize.getWidth()/2-buffer, (int)screenSize.getHeight()/2-buffer));
	        
	        fxPanel2.setSize(new Dimension((int)screenSize.getWidth()/2-buffer, (int)screenSize.getHeight()/2-buffer));

	        fxPanel4.setSize(new Dimension((int)screenSize.getWidth()/2, (int)screenSize.getHeight()/2));
	        
	        frame.setJMenuBar(menuBar);
	        
	        frame.getContentPane().setPreferredSize(screenSize);
	        frame.pack();
	        frame.setResizable(false);

	        // This will run initFX as JavaFX-Thread
	        Platform.runLater(new Runnable() {
	            @Override
	            public void run() {
	                initFX(fxPanel, "/src/graphs/polar/diagram.html");
	                initFX(fxPanel2,"/src/graphs/ranking/rankinggraph.html");
	                initFX(fxPanel4,"/src/graphs/bubble/bubblechart.html");
	            }
	        });
	    }
	   
	    // Creates a WebView and fires up a local web-page
	    private static void initFX(final JFXPanel fxPanel, String p) {
	        Group group = new Group();
	        Scene scene = new Scene(group);
	        fxPanel.setScene(scene);
	        
	        WebView webView = new WebView();
	        
	        group.getChildren().add(webView);

	        webView.setMinSize(screenSize.getWidth()/2, screenSize.getHeight()/2);
	        webView.setMaxSize(screenSize.getWidth()/2, screenSize.getHeight()/2);
	        
	        System.out.println("webView.size: " + screenSize.getWidth() + ", " + screenSize.getHeight());
	        
	        // Obtain the webEngine to navigate
	        WebEngine webEngine = webView.getEngine();
	        String contentURL = "";
	        

	        try{
	            contentURL = new URL("file:///" + dir + "/" + p).toExternalForm();
	        } catch (MalformedURLException e){}
	        
	        System.out.println("OPENING URL: " + contentURL);
	        System.out.println("CSS: " + dir + "/src/graphs/" + "style.css");
	        webView.getEngine().setUserStyleSheetLocation("file:///" + dir + "/src/graphs/" + "style.css");
	        webEngine.load(contentURL);
	    }
	    
	   public void actionPerformed(ActionEvent e){
		   System.out.println(e.toString());
	   }
}
