package Scatterplot;

/*
TablePanel.java

	This class implements the ViewController interface, thus has the same
	update method as all the other children of its Model.
	
	It contains a JTable that represents all the data in the data set
	The selected items are showed in the table
	
	I first implemented a row sorter, but that did not work with the
	way the data was sorted (unless sorted by index). A method that sorts the
	data by index internally should be implemented to allow for the user to sort by columns


* @Vladislavs Zaharovs 
*/

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

public class TablePanel extends JPanel implements ViewController{
	 private Model model; //references its model
	 private JTable table;// the table, representing data

	 ///constructor
	 public TablePanel(Model m) {
		 this.model = m;
		 

		//creates the table with data and labels with a new MyTableModel(model)
		 table = new JTable(new MyTableModel(model));
		 //the next line formats the size of the table, although to fit in well
		 //with the other panels
		 table.setPreferredScrollableViewportSize(new Dimension(300, 280));
		 table.setCellSelectionEnabled(false);//to allow selection through the table
		 JScrollPane scrollPane = new JScrollPane(table);
		 table.setFillsViewportHeight(false);
		 table.setColumnSelectionAllowed(false);
		 table.setRowSelectionAllowed(true);//now only full rows are selected
		 table.addMouseListener(new MouseButtonHandler());
		 
		 //The rowSorter was removed due to inconsistencies:
		 //
		 //table.setAutoCreateRowSorter(true);
		 
		 add(scrollPane);//adds the scrollpane to the panel
	 }
	 
	 //the View Controller update() method
	 //goes through all data, and selects rows in selectedRow (from model)
	 //removes everything else
	public void update() {
		((MyTableModel)table.getModel()).data = new Object[model.selectedSize()][3];		 
		 for (int i = 0; i < model.selectedSize(); i++){
			 ((MyTableModel)table.getModel()).data[i][0] = (Object) model.record(model.selectedIth(i)).get(2);
			 ((MyTableModel)table.getModel()).data[i][1] = (Object) model.record(model.selectedIth(i)).get(model.getXval());
			 ((MyTableModel)table.getModel()).data[i][2] = (Object) model.record(model.selectedIth(i)).get(model.getYval());}
		
		table.getTableHeader().getColumnModel().getColumn(1).setHeaderValue(model.getXvar());
		table.getTableHeader().getColumnModel().getColumn(2).setHeaderValue(model.getYvar());
		repaint();
		 //for (int i = 0; i < model.dataSize(); i++){
         	//if (model.isSelected(i)) table.addRowSelectionInterval(i, i);
         	//else table.removeRowSelectionInterval(i, i);
         	//}
		}
	
	//a Table model for the table
	class MyTableModel extends AbstractTableModel {
		Model model;
	    private String[] columnNames;
	    public Object[][] data;
	    
	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }

	    public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	    //MyTableModel constructor
	    public MyTableModel(Model m){
	    	super();
	    	model = m;
	    	
			 //creates Array of Arrays with all the data
			data = new Object[model.dataSize()+5000][3];		 
			 for (int i = 0; i < model.selectedSize(); i++){
				 for (int j = 0; j < 3; j++){
				 data[i][j] = (Object) model.record(i).get(j+2);}}
			//creates an Array with all the labels	 
			columnNames = new String[3];
			for (int i = 0; i < 3; i++){
				columnNames[i] = (String) model.fieldLabels().get(i+2);
				
			}
	    	
	    }
	    //needed to change values in the table
	    public void setValueAt(Object value, int row, int col) {
	        data[row][col] = value;
	        fireTableCellUpdated(row, col);
	    }
	}
	
	///Allows to select elements from the table
    class MouseButtonHandler extends MouseAdapter {

        public void mouseReleased(MouseEvent me) {
        	try{
    		int temp = table.getSelectedRows()[0];
    		
    		model.removeSelected(temp);
    		model.updateAll();//updates all other panels to ensure integrity
    		}catch (Error e){}

            }
    }
	}
