package application;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javafx.embed.swing.JFXPanel;
import java.awt.*;
import javafx.application.Platform;
import javafx.scene.*;
import java.net.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/** @author MichaelRoddy
 *  Loads a java window displaying a D3 graph via a html web page   
 */
public class Application {

		// Margin round the page as the D3 graphs draw right up to the edge
	    private static int margin = 30;
	    
	    // Used to open the specified file
	    private static String openPage = "/src/graphs/line.html";
	    private static String dir = "";
	    private static Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
	    
	    // Create a JFrame with a JButton and a JFXPanel containing the WebView.
	    private static void initAndShowGUI() {
	        
	        // This method is invoked on Swing thread
	        JFrame frame = new JFrame("FX");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        // Do the layout manually
	        frame.getContentPane().setLayout(null);
	        final JButton jButton = new JButton("Random Buttom");
	        
	        // This holds the web-page. I THINK it's similiar to a JPanel
	        final JFXPanel fxPanel = new JFXPanel();
	        
	        frame.add(jButton);
	        frame.add(fxPanel);
	        frame.setVisible(true);
	        
	        jButton.setSize(new Dimension(200, 27));
	        
	        // These are just random sizes. If you change these, you have to change
	        // them below too AND in each D3.html
	        fxPanel.setSize(new Dimension((int)screenSize.getWidth(), (int)screenSize.getHeight()-30));
	        fxPanel.setLocation(new Point(0, 27));
	        
	        frame.getContentPane().setPreferredSize(screenSize);
	        frame.pack();
	        frame.setResizable(false);

	        // This will run initFX as JavaFX-Thread
	        // (Not quite sure how this works... copied this bit)
	        Platform.runLater(new Runnable() {
	            @Override
	            public void run() {
	                initFX(fxPanel);
	            }
	        });
	    }
	    
	    // Creates a WebView and fires up a local web-page
	    private static void initFX(final JFXPanel fxPanel) {
	        Group group = new Group();
	        Scene scene = new Scene(group);
	        fxPanel.setScene(scene);
	        
	        WebView webView = new WebView();
	        
	        group.getChildren().add(webView);
	        
	        webView.setMinSize(screenSize.getWidth(), screenSize.getHeight());
	        webView.setMaxSize(screenSize.getWidth(), screenSize.getHeight());
	        
	        // Obtain the webEngine to navigate
	        WebEngine webEngine = webView.getEngine();
	        String contentURL = "";
	        
	        // Opens a local html file.
	        // The data is loaded through the html file, to change the data set,
	        // you'll have to change the data.csv file.
	        try{
	            contentURL = new URL("file:///" + dir + "/" + openPage).toExternalForm();
	        } catch (MalformedURLException e){}
	        System.out.println("OPENING URL: " + contentURL);
	        webEngine.load(contentURL);
	    }
	    
	    // Start application
	    public static void main(final String[] args) {
	        
	        // Read command line args and get current directory
	        //openPage = args[0];
	        dir = System.getProperty("user.dir");
	        
	        // Run!
	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                initAndShowGUI();
	            }
	        });
	    }

}
