package application;

import java.util.*;
import java.io.*;
import java.math.BigDecimal;

/** @author MichaelRoddy
 * 	Reads in and stores all data. Providing requests from the viewer.	*/
public class Model {

	// countries - maps a country name to an array of data
	// fields - maps a field name to the index of an array (data)
	// countryCodes - maps a country name to its country code
	private HashMap<String, BigDecimal[]> countries = new HashMap<String, BigDecimal[]>();
	private HashMap<String, Integer> fields = new HashMap<String, Integer>();
	private HashMap<String, String> countryCodes = new HashMap<String, String>();
	
	private String dir;
 	
	// Constructor & initialiser 
	public Model() {
		Scanner fileScanner = null;
		Scanner lineScanner = null;
		
		// Tracks whether the headers have been read in
		boolean headers = false;
		
		// Get the path to the project directory
		dir = System.getProperty("user.dir");

		// Try to open the data file.
		try {
			File dataSource = new File(dir + "/src/application/data/olympic_WHO_combined.csv");
			fileScanner = new Scanner(dataSource);
		} catch (FileNotFoundException e){ 
			System.out.println("CRITICAL ERROR - Data not found - Exiting..."); 
			System.exit(0); 
		}
		
		// If we have not gathered the headers yet, do so
		if(headers == false){
			int index = 0;
			lineScanner = new Scanner(fileScanner.nextLine());
			lineScanner.useDelimiter(",");
			while(lineScanner.hasNext()){
				String field = lineScanner.next();
				field = field.toLowerCase();
				fields.put(field, index);
				System.out.println(field + "  " + index);
				index++;
			}
		}
		
		// Gather the data for each country
		while(fileScanner.hasNextLine()){
			lineScanner = new Scanner(fileScanner.nextLine());
			lineScanner.useDelimiter(",");
			ArrayList<BigDecimal> data = new ArrayList<BigDecimal>();
			
			// Get the country name & code (first two fields)
			String country = lineScanner.next();
			String countryCode = lineScanner.next();
			countryCodes.put(country, countryCode);
			
			// Gather the rest of the data to an ArrayList.
			while(lineScanner.hasNext()){
				String s = lineScanner.next();
				BigDecimal value;
				if(s.equals("")){
					value = null; }
				else {
					value = new BigDecimal(s);
				}
				data.add(value);
			}
			
			// Place the country name & data into the hashmap
			Object[] objectArray = data.toArray();
			countries.put(country, Arrays.copyOf(objectArray, objectArray.length, BigDecimal[].class));
		}
	}
	
	// Takes in an arraylist of fields and prints them to a csv for a specific graph.
	public void export(String graph, ArrayList<String> reqFields){
		File file;
		FileWriter writer = null;
		
		try{
			file = new File(dir + "/src/graphs/" + graph + "/data.csv");
			file.createNewFile();
			writer = new FileWriter(file);
			System.out.println("Going to: " + dir + "/src/graphs/" + graph + "/data.csv");
		} catch (IOException e){ System.out.print("FILE IO EXCEPTION");}
		
	
		ArrayList<Integer> indices = new ArrayList<Integer>();
		/*
		for(String k : fields.keySet()){ System.out.println(k); }
		*/
		String header = "";
		for(String f : reqFields){
			//System.out.println("searching for: " + f);
			indices.add(fields.get(f));
			header += f + ",";
		}
		try{
			writer.write(header.substring(0, header.length()-1) + "\n");
		} catch (IOException e){System.out.println("Couldn't write"); }
		
		for(String country : countries.keySet()){
			String line = "";
			//System.out.println("indicies.size(): " + indices.size());
			for(Integer i : indices){
				//System.out.println(i);
				line += countries.get(country)[i.intValue()] + ",";
			}
			try{
				writer.write(line.substring(0, line.length()-1) + "\n");
			} catch (IOException e){System.out.print("FILE IO EXCEPTION");}
		}
		
		try{
		writer.flush();
		writer.close();
		} catch (IOException e){}
		
		
	}
}
