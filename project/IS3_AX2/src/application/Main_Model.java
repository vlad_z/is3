package application;

import java.util.*;
import java.io.*;
import java.math.BigDecimal;

/** @author MichaelRoddy
 * 	This is the main model for the program. It stores all the WHO/Olympic data in a set of
 *  hashmaps. The class accepts requests via the export method which generates new data for
 *  a graph on a given set of fields to export	*/
public class Main_Model {

	// countries - maps a country name to an array of data
	// fields - maps a field name to the index of an array (data)
	// countryCodes - maps a country name to its country code
	private HashMap<String, BigDecimal[]> countries = new HashMap<String, BigDecimal[]>();
	private HashMap<String, Integer> fields = new HashMap<String, Integer>();
	private HashMap<String, String> countryCodes = new HashMap<String, String>();
	
	// Location of the project (used for locating graphs directories)
	private String dir;
 	
	// Constructor & initialiser 
	public Main_Model() {
		
		Scanner fileScanner = null;
		Scanner lineScanner = null;
		
		// Tracks whether the headers have been read in
		boolean headers = false;
		
		// Get the path to the project directory
		dir = System.getProperty("user.dir");

		// Try to open the data file.
		try {
			File dataSource = new File(dir + "/src/application/data/olympic_WHO_combined.csv");
			fileScanner = new Scanner(dataSource);
		} catch (FileNotFoundException e){ 
			System.out.println("CRITICAL ERROR - Data not found - Exiting..."); 
			System.exit(0); 
		}
		
		// Begin reading in the main csv
		// If we have not gathered the headers yet, do so
		if(headers == false){
			int index = 0;
			lineScanner = new Scanner(fileScanner.nextLine());
			lineScanner.useDelimiter(",");
			lineScanner.next();	// Ignore first two entries (country & country code)
			lineScanner.next(); 
			while(lineScanner.hasNext()){
				String field = lineScanner.next();
				field = field.toLowerCase();
				fields.put(field, index);
				System.out.println(field + "  " + index);
				index++;
			}
		}
		
		// Gather the data for each country
		while(fileScanner.hasNextLine()){
			lineScanner = new Scanner(fileScanner.nextLine());
			lineScanner.useDelimiter(",");
			ArrayList<BigDecimal> data = new ArrayList<BigDecimal>();
			
			// Get the country name & code (first two fields)
			String country = lineScanner.next();
			String countryCode = lineScanner.next();
			countryCodes.put(country, countryCode);
			
			// Gather the rest of the data to an ArrayList.
			while(lineScanner.hasNext()){
				String s = lineScanner.next();
				BigDecimal value;
				if(s.equals("")){
					value = null; }
				else {
					value = new BigDecimal(s);
				}
				data.add(value);
			}
			
			// Place the country name & data into the hashmap
			Object[] objectArray = data.toArray();
			countries.put(country, Arrays.copyOf(objectArray, objectArray.length, BigDecimal[].class));
		}
	}
	
	// Takes in an arraylist of fields and prints them to a csv for a specific graph.
	public void export(String graph, ArrayList<String> reqFields){
		
		File file;
		FileWriter writer = null;

		// Try to create the file in the correct directory (specified by 'graph')
		try{
			file = new File(dir + "/src/graphs/" + graph + "/data.csv");
			file.createNewFile();
			writer = new FileWriter(file);
			System.out.println("Going to: " + dir + "/src/graphs/" + graph + "/data.csv");
		} catch (IOException e){ System.out.print("FILE IO EXCEPTION");}
		
		
		// List of field (indices) we have to grab
		ArrayList<Integer> indices = new ArrayList<Integer>();
		String header = ""; // This is the first line of the csv
		for(String f : reqFields){
			indices.add(fields.get(f));
			header += f + ",";
		}
		
		// Attempt to write the header line into the csv
		try{
			writer.write("country," + header.substring(0, header.length()-1) + "\n");
		} catch (IOException e){System.out.println("Couldn't write"); }
		
		// For each country, get the appropriate fields from the dataset
		for(String country : countries.keySet()){
			String line = country; // Start each line with the country name
			
			// For each field... get the data
			for(Integer i : indices){
				System.out.println(countries.get(country)[i]);
				line += "," + countries.get(country)[i.intValue()];
			}
			
			// Write the next line of the csv
			try{
				writer.write(line/*.substring(0, line.length()-1) */+ "\n");
			} catch (IOException e){System.out.print("FILE IO EXCEPTION");}
		}
		
		try{
			writer.flush();
			writer.close();
		} catch (IOException e){}
	}
}
