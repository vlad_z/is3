package application;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javafx.embed.swing.JFXPanel;
import java.awt.*;
import javafx.application.Platform;
import javafx.scene.*;
import java.net.*;

import java.awt.event.*;
import javax.swing.*;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import java.util.*;
import Scatterplot.*;

/** @author MichaelRoddy
 *  Loads a java window displaying D3 graphs and a java scatterplot.
 */
public class Viewer {
	   
		private static Main_Model model;
	    private static String dir = "";		// Location of this project
	    private static Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
	    
	    // Start application
	    public static void main(final String[] args) {

	    	model = new Main_Model();
	        dir = System.getProperty("user.dir");
	        
	        // Run
	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                initAndShowGUI();
	            }
	        });
	    }
	    
	    // Create a JFrame with a JButton and a JFXPanel containing the WebView.
	    private static void initAndShowGUI(){
	    	
	    	// Pan for displaying javascript graphs
	        final JFXPanel fxPanel = new JFXPanel();
	        final JFXPanel fxPanel2 = new JFXPanel();	        

	        // Define components needed for the menu
	    	JMenuBar menuBar;
	    	JMenu fileMenu, menu, lSubmenu, cSubmenu;
	    	JMenuItem exitMenuItem;
	    	JMenuItem pMenuItemPop, pMenuItemGdp, pMenuItemVar3, pMenuItemVar4;
	    	JMenuItem cMenuItemPop;
	    	
	    	// Initialise menus
	    	menuBar = new JMenuBar();
	    	fileMenu = new JMenu("File");
	    	menu = new JMenu("Graphs");
	    	menuBar.add(fileMenu);
	    	menuBar.add(menu);
	    	lSubmenu = new JMenu("Polar Diagram");
	    	cSubmenu = new JMenu("Choropleth");
	    	
	    	// Add submenu option, add listener and add to menu.
	    	exitMenuItem = new JMenuItem("Exit");
	        exitMenuItem.addActionListener(new ActionListener() {
	            @Override public void actionPerformed( ActionEvent aActionEvent ) {
	              System.exit(0);
	            }
	        });
	        fileMenu.add(exitMenuItem);
	    	
	        // Create polar diagram menu items, then add all relevant components and listeners
	    	pMenuItemPop = new JMenuItem("Variable: population");
	        pMenuItemPop.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("polar:population");
	                ArrayList<String> f = new ArrayList<String>();
	                f.add("population total");
	                f.add("gold");
	                model.export("polar", f);	// Request data from model
	                initAndShowGUI();
	            }
	        });
	    	lSubmenu.add(pMenuItemPop);
	    	pMenuItemGdp = new JMenuItem("Variable: gdp");
	        pMenuItemGdp.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("polar:gdp2011");
	                ArrayList<String> f = new ArrayList<String>();
	                f.add("gdp2011");
	                f.add("gold");
	                model.export("polar", f);	// Request data from model
	                initAndShowGUI();
	            }
	        });
	    	lSubmenu.add(pMenuItemGdp);
	    	pMenuItemVar3 = new JMenuItem("Variable: adult literacy rate");
	        pMenuItemVar3.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("polar:adult literacy rate");
	                ArrayList<String> f = new ArrayList<String>();
	                f.add("adult literacy rate");
	                f.add("gold");
	                model.export("polar", f);	// Request data from model
	                initAndShowGUI();
	            }
	        });
	    	lSubmenu.add(pMenuItemVar3);
	    	pMenuItemVar4 = new JMenuItem("Variable: team size");
	        pMenuItemVar4.addActionListener(new ActionListener() {
	        	 
	            public void actionPerformed(ActionEvent e)
	            {
	                //Execute when button is pressed
	                System.out.println("polar:teamsize");
	                ArrayList<String> f = new ArrayList<String>();
	                f.add("teamsize");
	                f.add("gold");
	                model.export("polar", f);	// Request data from model
	                initAndShowGUI();
	            }
	        });
	    	lSubmenu.add(pMenuItemVar4);
	    	cMenuItemPop = new JMenuItem("Currently unimplemented");
	    	cSubmenu.add(cMenuItemPop);

	    	// Add the submenus to the main menu bar
	    	menu.add(lSubmenu);
	    	menu.add(cSubmenu);
	    
	    	// Begin creating the complete frame
	    	int buffer = 10;
	        JFrame frame = new JFrame("FX");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.getContentPane().setLayout(new GridLayout(2,1));

	        // Add the two jfxpanels to the top of the page
	        JPanel top = new JPanel();
	        top.setLayout(new GridLayout(1,2));;
	        top.add(fxPanel);
	        top.add(fxPanel2);
	        frame.setVisible(true);

	        // Add the top panel and the scatterplot into their respective positions
	        frame.add(top);
	        frame.add(new Scatterplot(dir + "/src/Scatterplot/who.csv"));
	        
	        // Make the jfxpanel sizes conform the the size of the screen
	        fxPanel.setSize(new Dimension((int)screenSize.getWidth()/2-buffer, (int)screenSize.getHeight()/2-buffer));
	        fxPanel2.setSize(new Dimension((int)screenSize.getWidth()/2-buffer, (int)screenSize.getHeight()/2-buffer));

	        // Final additions to the frame
	        frame.setJMenuBar(menuBar); 
	        frame.getContentPane().setPreferredSize(screenSize);
	        frame.pack();
	        frame.setResizable(false);

	        // This runs initFX as JavaFX-Thread
	        Platform.runLater(new Runnable() {
	            @Override
	            public void run() {
	                initFX(fxPanel, "/src/graphs/polar/diagram.html");
	                initFX(fxPanel2,"/src/graphs/ranking/rankinggraph.html");
	            }
	        });
	    }
	   
	    // Create the jfxpanels, add web views and open up graphs into them
	    private static void initFX(final JFXPanel fxPanel, String graph) {
	        Group group = new Group();
	        Scene scene = new Scene(group);
	        fxPanel.setScene(scene);
	        
	        WebView webView = new WebView();
	        group.getChildren().add(webView);

	        webView.setMinSize(screenSize.getWidth()/2, screenSize.getHeight()/2);
	        webView.setMaxSize(screenSize.getWidth()/2, screenSize.getHeight()/2);
	        
	        WebEngine webEngine = webView.getEngine();
	        String contentURL = "";
	        
	        // Create a path to the graph from the graph name and project directory
	        try{
	            contentURL = new URL("file:///" + dir + "/" + graph).toExternalForm();
	        } catch (MalformedURLException e){}
	        
	        // Link in a standard css file (just adds margins to the graph)
	        webView.getEngine().setUserStyleSheetLocation("file:///" + dir + "/src/graphs/" + "style.css");
	        webEngine.load(contentURL);
	    }
}
