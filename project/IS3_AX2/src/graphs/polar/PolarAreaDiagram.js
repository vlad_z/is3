var loadViz = function(){
    
    window.chart = new Highcharts.Chart({
        
        chart: {
            renderTo: '#container',
            polar: true
        },
        
        title: {
            text: 'Highcharts Polar Chart'
        },
        
        pane: {
            startAngle: 0,
            endAngle: 360
        },
    
        xAxis: {
            tickInterval: 45,
            min: 0,
            max: 360,
            labels: {
                formatter: function () {
                    return this.value + '°';
                }
            }
        },
            
        yAxis: {
            min: 0
        },
        
        plotOptions: {
            series: {
                pointStart: 0,
                pointInterval: 45
            },
            column: {
                pointPadding: 0,
                groupPadding: 0
            }
        },
    
        series: [{
            type: 'column',
            name: 'Column',
            data: [8, 7, 6, 5, 4, 3, 2, 1],
            pointPlacement: 'between'
        }, {
            type: 'line',
            name: 'Line',
            data: [1, 2, 3, 4, 5, 6, 7, 8]
        }, {
            type: 'area',
            name: 'Area',
            data: [1, 8, 2, 7, 3, 6, 4, 5]
        }]
    });
};
// var timeseries, sdat, series, minVal = 0, maxVal, radius, radiusLength;
// var size;
// var w = window.innerWidth, h = window.innerHeight, axis = 25, time = 10, ruleColor = '#CCC';
// var test;
// var check;

// var vizPadding = {
//     top: 50,
//     right: 50,
//     bottom: 50,
//     left: 50
// };
// var numticks;
// var viz, vizBody,keys;
// var loadViz = function(){
//   check = d3.text("data.csv","text/csv",function(text){
//   var temp = d3.csv.parseRows(text);
//   var headers = temp.slice(0,1);

//   var sorted =[];
//   var maximum = 0;
//   for(var i=0; i<temp.length;i++){
  
//     if(parseInt(temp[i][2],10)>0){
//       sorted.push(temp[i]);
//       // if(parseInt(temp[1][2],10)>maximum)
//       //   maximum = (temp[i][2];
//     }
//   }
//   axis = sorted.length;
//   test = [];
//   for(var i=0;i<sorted.length;i++){
//     test.push(sorted[i][0]);
//     if(parseInt(sorted[i][2],10)>maximum){
//       maximum = parseInt(sorted[i][2],10)
//     }
//   }
//   maxVal = maximum;
//   numticks = maxVal / 1;


//   loadData(test,sorted);
//   buildBase();
//   setScales();
//   drawBars(0);
//   addLineAxes();
//   addCircleAxes();
// });

// };

// var testLoad = function(test2){
//   d3.select("#output")
//     .text("Countries by "+test2);

// };
// var loadData = function(test,sorted){
//     // var randomFromTo = function randomFromTo(from, to){
//     //    return Math.random() * (to - from) + from;
//     // };

//     timeseries = [];
//     sdat = [];
//     keys = test;
//     var temporary = keys.slice(keys.length-1,keys.length);
//     keys = keys.slice(0,keys.length);
//     keys.reverse();
//     keys.push(temporary);
//     keys.reverse();

 


//     // if(temp!=null)
//     //   keys = temp[0] ;

//     for (j = 0; j < time; j++) {
//         series = [[]];
//         for (i = 0; i < axis; i++) {
//             series[0][i] =parseInt(sorted[i][2],10);
//         }
//         // This fills in the line 
// /*        for (i = 0; i < series.length; i += 1) {
//             series[i].push(series[i][0]);
//         }
// */        
//         for (i=0; i<=numticks; i++) {
//             sdat[i] = (maxVal/numticks) * i;
//         }
        
//         timeseries[j] = series;
//     }
// };

// var buildBase = function(){
//     viz = d3.select("#radial")
//                 .append('svg:svg')
//                     .attr('width', w)
//                     .attr('height', h);

//     viz.append("svg:rect")
//             .attr('x', 0)
//             .attr('y', 0)
//             .attr('height', 0)
//             .attr('width', 0)
//             .attr('height', 0);
    
//     vizBody = viz.append("svg:g")
//         .attr('id', 'body');
// };

// setScales = function () {
//   var heightCircleConstraint,
//       widthCircleConstraint,
//       circleConstraint,
//       centerXPos,
//       centerYPos;

//   //need a circle so find constraining dimension
//   heightCircleConstraint = h - vizPadding.top - vizPadding.bottom;
//   widthCircleConstraint = w - vizPadding.left - vizPadding.right;
//   circleConstraint = d3.min([heightCircleConstraint, widthCircleConstraint]);

//   radius = d3.scale.linear().domain([0, maxVal])
//       .range([0, (circleConstraint / 2)]);
//   radiusLength = radius(maxVal);

//   //attach everything to the group that is centered around middle
//   centerXPos = widthCircleConstraint / 2 + vizPadding.left;
//   centerYPos = heightCircleConstraint / 2 + vizPadding.top;

//   vizBody.attr("transform", "translate(" + centerXPos + ", " + centerYPos + ")");
// };

// addCircleAxes = function() {
//     var radialTicks = radius.ticks(numticks), circleAxes, i;
        
//     vizBody.selectAll('.circle-ticks').remove();
        
//     circleAxes = vizBody.selectAll('.circle-ticks')
//       .data(sdat)
//       .enter().append('svg:g')
//       .attr("class", "circle-ticks");

//     circleAxes.append("svg:circle")
//       .attr("r", function (d, i) { return radius(d); })
//       .attr("class", "circle")
//       .style("stroke", ruleColor)
//       .style("opacity", 0.7)
//       .style("fill", "none");

//     circleAxes.append("path")
//         .attr("fill", "green")
//         .attr("d", bar)
//         .style("opacity", 0.4);

//     circleAxes.append("svg:text")
//       .attr("text-anchor", "left")
//       .attr("dy", function (d) { return -1 * radius(d); })
//       .text(String);
// }

// addLineAxes = function () {
//   var radialTicks = radius.ticks(numticks), lineAxes;

//   vizBody.selectAll('.line-ticks').remove();

//   lineAxes = vizBody.selectAll('.line-ticks')
//       .data(keys)
//       .enter().append('svg:g')
//       .attr("transform", function (d, i) {
//           return "rotate(" + ((i / axis * 360) - 90) +
//               ")translate(" + radius(maxVal) + ")";
//       })
//       .attr("class", "line-ticks");



//   lineAxes.append('svg:line')
//       .attr("x2", -1 * radius(maxVal))
//       .style("stroke", ruleColor)
//       .style("opacity", 1)
//       .style("fill", "yellow");

//   lineAxes.append('svg:text')
//       .text(function(d,i){ return keys[i]; })
//       .attr("text-anchor", "end")
// //      .attr("transform", function (d, i) {
// //          return (i / axis * 360) < 180 ? null : "rotate(90)";
// //      });
// };

// var draw = function (val) {
//   var groups,
//       lines,
//       linesToUpdate;

//   groups = vizBody.selectAll('.series')
//       .data(timeseries[val]);
//   groups.enter().append("svg:g")
//       .attr('class', 'series')
//       .style('fill', "green")
//       .style('stroke', "green");
      
//   groups.exit().remove();

//   lines = groups.append('svg:path')
//       .attr("class", "line")
//       .attr("id", "userdata")
//       .attr("d", d3.svg.area.radial()
//           .radius(function (d) { return 0; })
//           .angle(function (d, i) { return (i / axis) * 2 * Math.PI; }))
//       .style("stroke-width", 3)
//       .style("fill", "blue")
//       .style("opacity", 0.4);

//   lines.attr("d", d3.svg.area.radial()
//       .outerRadius(function (d) { return radius(d); })
//       .innerRadius(function(d) { return 0; })
//       .angle(function (d, i) { return (i / axis) * 2 * Math.PI; }));
// };


// var drawBars = function(val) {
//     var groups, bar;
//     pie = d3.layout.pie().value(function(d) { return d; }).sort(null);
//     d = [];
//     for(i = 0; i<timeseries[val][0].length; i++) { d.push(1); }

//     groups = vizBody.selectAll('.series')
//         .data([d]);

//     groups.enter().append("svg:g")
//         .attr('class', 'series')
//         .style('fill', "blue")
//         .style('stroke', "black");


      
//     groups.exit().remove();
    
//     bar = d3.svg.arc()
//         .innerRadius( 0 )
//         .outerRadius( function(d,i) { return radius( timeseries[val][0][i] ); });
    

//     arcs = groups.selectAll(".series g.arc")
//         .data(pie)
//         .enter()
//             .append("g")
//                 .attr("class", "attr");
                

//     arcs.append("path")
//         .attr("fill", "#B0171F")
//         .attr("d", bar)
//         .style("opacity", 0.4);

// }

// function redraw( val ) {        
//     vizBody.selectAll('#userdata').remove();
//     drawBar( val );
// }