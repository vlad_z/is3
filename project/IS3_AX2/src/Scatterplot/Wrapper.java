package Scatterplot;

import javax.swing.JFrame;

/* 
   Creates a frame with the actual Scatter Plot, using a hardcoded file
 */

public class Wrapper {
  public static void main(String args[]) {
	
	  JFrame mainFrame = new JFrame();
	  mainFrame.add(new Scatterplot("who.csv"));
      mainFrame.setTitle("WHO Olympics Data");
      mainFrame.setVisible(true);
      mainFrame.setSize(1000,400);
    

  }
}





