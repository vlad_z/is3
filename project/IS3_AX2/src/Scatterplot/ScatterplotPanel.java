package Scatterplot;

/*
 ScatterplotPanel.java
 Instrumented scatterplot for displaying financial data
 
 This is a heavily modified version of the original ScatterplotPanel
 
 I have added Buttons to change the location of the view on the Scatterplot View
 and replaced the text fields with sladers for scaling
 */
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

class ScatterplotPanel
        extends JPanel
        implements ViewController {

    private Model model;
    private ScatterplotView scatterplotView;
    private JSlider xSlider, ySlider; //Sliders for scaling
    AxisListPanel axisPanel;
    private int xAxisIndex = 3, yAxisIndex = 4;
    private JButton panUp, panDown, panLeft,panRight, panReset;//panning buttons
    
    

    public ScatterplotPanel(Model m) {
    	this.model = m;
    	axisPanel = new AxisListPanel(this, model.numericFieldLabels());
        double scale = 10.0; 	/* first set an initial scale */
        
        // Create panning buttons
        panUp = new JButton("Up");
        panUp.setPreferredSize(new Dimension(160, 30));
        panDown = new JButton("Down");
        panDown.setPreferredSize(new Dimension(160, 30));
        panLeft = new JButton("L");
        panLeft.setPreferredSize(new Dimension(65, 30));
        panRight = new JButton("R");
        panRight.setPreferredSize(new Dimension(65, 30));
        panReset = new JButton();
        panReset.setPreferredSize(new Dimension(30, 30));
        
        panUp.addActionListener(new ButtonListener());
        panDown.addActionListener(new ButtonListener());
        panLeft.addActionListener(new ButtonListener());
        panRight.addActionListener(new ButtonListener());
        panReset.addActionListener(new ButtonListener());
        
        
        /* Create and add sliders to edit the scale for X and Y */
        xSlider = new JSlider(JSlider.HORIZONTAL, 0, 500, 50);
        ySlider = new JSlider(JSlider.HORIZONTAL, 0, 500, 50);
        
        xSlider.setMajorTickSpacing(250);
        xSlider.setMinorTickSpacing(50);
        xSlider.setPaintTicks(true);
        xSlider.setPaintLabels(true);
        ySlider.setMajorTickSpacing(250);
        ySlider.setMinorTickSpacing(50);
        ySlider.setPaintTicks(true);
        ySlider.setPaintLabels(true);

        /* Set up listeners for when someone moves the sliders */   
        xSlider.addChangeListener(new SliderListener());
        ySlider.addChangeListener(new SliderListener());

        
        
        ///Setting up te layout
       ///
        JPanel scaleFieldPanel = new JPanel(/*new BorderLayout()*/);
        JPanel sliderPanel = new JPanel(new BorderLayout(2,2));
        sliderPanel.add("North", xSlider);
        sliderPanel.add("Center", ySlider);
        sliderPanel.setPreferredSize(new Dimension(80,20));
        
        JPanel panPanel = new JPanel(new BorderLayout());
        panPanel.add("North", panUp);
        panPanel.add("South", panDown);
        panPanel.add("West", panLeft);
        panPanel.add("East", panRight);
        panPanel.add("Center", panReset);
        panPanel.setSize(new Dimension(160, 90));
        
        JPanel panAndScale = new JPanel(new BorderLayout(5, 5));
        panAndScale.setPreferredSize(new Dimension(80,85));
        panAndScale.add("Center", sliderPanel);
        panAndScale.add("West", panPanel);
        panAndScale.add("East", axisPanel);


        /* Create the 2d view and load it with data */
        scatterplotView = new ScatterplotView(model, scale);

        /* Set up a layout manager to handle this panel's subcomponents */
        setLayout(new BorderLayout());
        
        add("North", panAndScale);
        add("West", scaleFieldPanel);
        add("Center", scatterplotView);
        
    }
    
    public int getXvar(){return xAxisIndex;}
    public int getYvar(){return yAxisIndex;}

    /* 
     Inner class, to handle events from the scaleSlider listener.
     */
    public class SliderListener implements ChangeListener{
    	  public void stateChanged(ChangeEvent event){
    		  	JSlider slider = (JSlider) event.getSource();
    		  	//scaleVal is incremented by 0.1 to avoid an 
    		  	//infinitely small scale (showing only a dot)
    		  	double scaleVal = ((double) slider.getValue())/5 + 0.1;
    		  	//determines the slider and updates its scale
    	        if (slider.equals(xSlider)) {
    	        	scatterplotView.setXScale(scaleVal);
    	        	
    	        } else 
    	        	{scatterplotView.setYScale(scaleVal);
    	        	}	
    	  }	
		}
    	 
    //Actionlistener for the buttons to move the view
    //Each press will move by 20 units in the specified direction
    public class ButtonListener implements ActionListener{
  	  public void actionPerformed(ActionEvent e){
  		JButton b = (JButton) e.getSource();
  		if (b.equals(panUp)) {
  			scatterplotView.setYP(scatterplotView.getYP() + 20);
  			scatterplotView.repaint();
  		}
  		else if (b.equals(panDown)) {
  			scatterplotView.setYP(scatterplotView.getYP() - 20);
  			scatterplotView.repaint();
  		}
  		else if (b.equals(panLeft)) {
  			scatterplotView.setXP(scatterplotView.getXP() + 20);
  			scatterplotView.repaint();
  		}
  			
  		else if (b.equals(panRight)){
  			scatterplotView.setXP(scatterplotView.getXP() - 20);
			scatterplotView.repaint();
  			}
  		else { //the middle reset button sets the view back to the middle
  			scatterplotView.setXP(0);
  			scatterplotView.setYP(0);
			scatterplotView.repaint();
  		}
  		 }
    }
 

    //Updates based on the ArrayList seletedRows 
    //this is the new ViewController interface method
    public void update() {
        scatterplotView.update();  /* pass it down to the scatterplotView */
    }

    // usually called from AxisListPanel 
    public void updateXAxis(String itemLabel) {
        // get index of the field called itemLabel from the model
        xAxisIndex = model.indexOfField(itemLabel);

        // update the scatterplot based on the x and y field indices
        scatterplotView.loadData(xAxisIndex, yAxisIndex);
    }

    public void updateYAxis(String itemLabel) {
        // get index of the field called itemLabel from the source domain
        yAxisIndex = model.indexOfField(itemLabel);

        // update the scatterplot based on the x and y field indices
        scatterplotView.loadData(xAxisIndex, yAxisIndex);
    }
    public void updateAll(){model.updateAll();}
    
} /* end ScatterplotPanel */
