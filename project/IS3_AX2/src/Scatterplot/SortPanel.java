package Scatterplot;
/*
SortPanel.java

This is a panel that contains tools for sorting data


* @author Vladislavs Zaharovs (some code was taken from Java tutorials)
*/
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Scatterplot.ScatterplotPanel.ButtonListener;
import Scatterplot.ScatterplotPanel.SliderListener;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class SortPanel extends JPanel{

    private Model model; 
    private JButton reset, selectAll; // clears model.selectedRows, repaints
    
    
    public SortPanel(Model m) {
        this.model = m;       
        
        // Create reset button and add listeners
        reset = new JButton("Reset"); 
        selectAll = new JButton("Select All");
        reset.addActionListener(new ButtonListener());
        selectAll.addActionListener(new ButtonListener());
        
        
        ///
        ///Button panel
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 2, 2, 2));
        buttonPanel.add(selectAll);
        buttonPanel.add(reset);
        buttonPanel.setPreferredSize(new Dimension(80,40));
        ///
       
        ///
        
        /* Set up a layout manager to handle this panel's subcomponents */
        setLayout(new BorderLayout());

        add(buttonPanel);
    }
    


    
    //Actionlistener for the buttons to move the view
    // invokes the Model methods
    public class ButtonListener implements ActionListener{
  	  public void actionPerformed(ActionEvent e){
  		JButton b = (JButton) e.getSource();
  		if (b.equals(reset)) {
  			model.resetAll();
  		}
  		else model.selectAll();
  		model.updateAll();
  		}
  	  }
    
    
    //Slider listener for the 3 sliders
    //I tried using min/max sliders but it felt much more confusing
    //as there was too many of them and the screen felt clogged
   
    
    ///Sets up the combo box listeners works similar to the textFields, but with set values
	 private class ComboListener implements ActionListener{
		    public void actionPerformed(ActionEvent e) {
		        JComboBox cb = (JComboBox)e.getSource();
		        String label = (String)cb.getSelectedItem();
				 if (label != null) {
			                for (int i = model.selectedSize() - 1; i >= 0; i--){
			                	String name = 
			                	(String) model.record(model.selectedIth(i)).get(9);
			                	if (!name.equals(label)) model.removeSelected(i);
			                	}
					  }
				 model.updateAll();
		    }}
}
